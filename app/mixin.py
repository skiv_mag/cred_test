#!/usr/bin/env python
# -*- coding: utf-8 -*-


from datetime import datetime
from flask.ext.sqlalchemy import orm

from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property

from extensions import db


class BaseMixin(object):
    """ Base mixin
    """
    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    def save(self, commit=True, **kwargs):
        db.session.add(self)
        commit and db.session.commit()
        return self

    def delete(self, commit=True):
        db.session.delete(self)
        commit and db.session.commit()

    def __repr__(self):
        return "<%s:%s>" % (self.__class__.__name__,
                            self.id)

class CRUDMixin(BaseMixin):
    """ Basic CRUD mixin
    """

    @declared_attr
    def id(cls):
        return db.Column(db.Integer, primary_key=True)

    # @declared_attr
    # def created_at(cls):
    #     return db.Column(db.DateTime, default=datetime.utcnow)

    @classmethod
    def get(cls, id):
        return cls.query.get(id)

    @classmethod
    def create(cls, commit=True, **kwargs):
        return cls(**kwargs).save(commit)

    def update(self, commit=True, **kwargs):
        return self._setattrs(**kwargs).save(commit)

    def as_dict(self, api_fields=None, exclude=None):
        """ method for building dictionary for model value-properties filled
            with data from mapped storage backend
        """
        exclude = set(exclude or [])
        api_fields = api_fields or getattr(self, 'api_fields', [])

        column_properties = [p.key for p in self.__mapper__.iterate_properties
                                if isinstance(p, orm.ColumnProperty)]

        column_properties.extend(api_fields)
        exportable_fields = set(column_properties)

        # convert undescored fields:
        fields = set(map(lambda f: f.strip('_'), exportable_fields))

        exportable_fields = fields - exclude

        results = dict()

        for field in exportable_fields:
            attr = getattr(self, field)
            # process callable objects
            value = hasattr(attr, '__call__') and attr() or attr
            results[field] = value

        return results

    def _setattrs(self, **kwargs):
        for k, v in kwargs.iteritems():
            k.startswith('_') and raise_value('Underscored values are not'
                                              ' allowed')
            setattr(self, k, v)

        return self