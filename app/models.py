#!/usr/bin/env python
# -*- coding: utf-8 -*-

from extensions import db
from mixin import CRUDMixin
from config import Configuration as CFG

from werkzeug.security import generate_password_hash, check_password_hash


class Cards(db.Model, CRUDMixin):
    card_number = db.Column(db.String)
    pin_code = db.Column(db.String)
    amount = db.Column(db.Float)
    is_blocked = db.Column(db.Boolean)
    login_attempts = db.Column(db.Integer)

    def __init__(self, **kwargs):
        kwargs["pin_code"] = generate_password_hash(kwargs["pin_code"])
        super(Cards, self).__init__(**kwargs)

    def check_password(self, password):
        return check_password_hash(self.pin_code, password)


class Operations(db.Model, CRUDMixin):
    card_id = db.Column(db.Integer, db.ForeignKey('cards.id'))
    operation_type = db.Column(db.String)
    amount = db.Column(db.Float)
