#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Response, url_for, jsonify as flask_jsonify
from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException
from werkzeug.utils import secure_filename

from pprint import pprint
import json
import time
import datetime
import os.path as op

from config import Configuration as CFG


def print_obj(object_name):
    try:
        pprint({x: getattr(object_name, x) for x in dir(object_name)})
    except:
        for x in dir(object_name):
            try:
                print {x: getattr(object_name, x)}
            except:
                'cant print %s' % x

def json_handler(obj):
    if isinstance(obj, datetime.date) or isinstance(obj, datetime.datetime):
        return obj.isoformat()
    else:
        return None


def as_dict_many(obj_list, **kwargs):
    return map(lambda x: x.as_dict(**kwargs), obj_list)


def get_current_time():
    dt_obj = datetime.datetime.now()
    return dt_obj.strftime("%Y-%m-%d %H:%M:%S")