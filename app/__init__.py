#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from extensions import db
from views import main

app = Flask(__name__, template_folder='../templates',
            static_folder='../static')
app.config.from_object('config.Configuration')

# init db
db.app = app
db.init_app(app)


# register blueprint
app.register_blueprint(main, url_prefix='/')