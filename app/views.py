#!/usr/bin/env python
# -*- coding: utf-8 -*-

from models import Cards, Operations
from utils import print_obj, get_current_time
from flask import Blueprint, redirect, request, Response, render_template, url_for, g, session, flash
from extensions import db

import datetime

main = Blueprint('main', 'main')


@main.before_request
def before_request():
    if "card" not in session and request.endpoint not in('main.login', 'main.test', 'main.error_page'):
        return redirect(url_for("main.login"))



@main.route('/', methods = ["GET", "POST"])
def login():
    session.clear()
    if request.method == "POST" and request.form:
        card_number = request.form["creditcard"]
        card = Cards.query.filter_by(card_number=card_number, is_blocked = False).first()
        if not card:
            return redirect(url_for("main.error_page", message = "Can't find valid card"))
        session["card"] = card.id
        return redirect(url_for("main.pin_enter"))
    session.clear()
    return render_template("login.html")


@main.route('pin/', methods = ["GET", "POST"])
def pin_enter():
    card = Cards.get(session["card"])
    print card
    if request.method == "POST":
        if not card.check_password(request.form["pin"]):
            if card.login_attempts >= 4:
                card.update(is_blocked=True)
                return redirect(url_for("main.error_page", message ="Card Blocked"))
            la = card.login_attempts + 1
            card.update(login_attempts=la)
            return redirect(url_for("main.pin_enter"))
        else:
            card.update(login_attempts=0)
            return redirect(url_for("main.menu"))
    return render_template("pin.html")


@main.route('menu/')
def menu():
    return render_template("menu.html")


@main.route('balance/')
def balance():
    card = Cards.get(session["card"])
    current_date = get_current_time()
    return render_template("balance.html", card=card, current_date=current_date)


@main.route('get_money/', methods = ["GET", "POST"])
def get_money():
    card = Cards.get(session["card"])
    if request.method == "POST":
        money = float(request.form["money"])
        if card.amount <= money:
            return redirect(url_for("main.error_page", message="Not enough money"))
        new_sum = card.amount - money
        card.update(amount=new_sum)
        Operations.create(operation_type="get_money",amount=money, card_id=card.id)
        session["money"] = money
        return redirect(url_for("main.operation_results"))
    return render_template("money.html")


@main.route('operation_results/')
def operation_results():
    card = Cards.get(session["card"])
    money = session.pop("money")
    current_date = get_current_time()
    return render_template("operation_results.html", card=card, current_date=current_date, money=money)


@main.route('error/<message>')
def error_page(message):
    return render_template('404.html', message=message), 404


@main.route('create_test_data/')
def test():
    Cards.create(
        pin_code='1111', 
        card_number='1111-1111-1111-1111', 
        amount=2568.10, 
        is_blocked=False, 
        login_attempts =0
    )
    Cards.create(
        pin_code='1234', 
        card_number='2222-5555-8888-8888', 
        amount=500.10, 
        is_blocked=False, 
        login_attempts =0
    )
    return 'ok'
