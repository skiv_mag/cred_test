#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import app, db

if __name__ == '__main__':
    db.create_all()
    app.run()
